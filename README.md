# Klontong Frontend

> Catalog product
 
## Table of contents

> * [Klontong Frontend](#Klontong-frontend)
>   * [Table of contents](#table-of-contents)
>   * [Installation](#installation)
>     * [Clone Backend](#clone-backend)
>     * [Clone](#clone)
>     * [Change Branch](#change-branch)
>     * [Install Depedencies](#install-depedencies)
>     * [Run server development](#run-server-development)
>   * [Dependencies](#dependencies)
>   * [License](#license)
 
## Installation

### Clone Backend
```
$ Open source code in this link https://gitlab.com/rizkysyarif/klontong-backend.git
```

### Clone
```
$ git clone https://gitlab.com/rizkysyarif/klontong-frontend.git
$ cd klontong-frontend
```

### Change Branch
```
$ git checkout master
```

### Install Depedencies
```
npm install
```

### Run server development
```
npm start
```

## Dependencies
```
- Axios
- React Redux
- Redux Toolkit
- Material UI
```

## License
MIT

